% Robotics: Estimation and Learning 
% WEEK 3
% 
% Complete this function following the instruction.
% ranges is 1081,K lidar sensor readings.
%     e.g. ranges(:,k) is the lidar measurement (in meter) at time index k.
% scanAngles is 1081,1 array containing at what angles (in radian) the 1081-by-1 lidar
%     values ranges(:,k) were measured. This holds for any time index k. The
%     angles are with respect to the body coordinate frame.
% pose is 3,K array containing the pose of the mobile robot over time.
%     e.g. pose(:,k) is the [x(meter),y(meter),theta(in radian)] at time index k.
function theMap = occGridMapping(ranges, scanAngles, pose, param)

    %
    % the number of grids for 1 meter.
    resolution = param.resol;
    % the map size in pixels
    theMap = zeros(param.size);
    % % the origin of the map in pixels
    mapOrigin_x = param.origin(1);
    mapOrigin_y = param.origin(2);

    % number of poses in the measurements
    K = size(pose,2);

    % number of ray measurements in each pose
    n_angles = size(scanAngles,1);

    for time_ix = 1:K

        for angle_ix = 1:n_angles

            % world coordinates
            x = pose(1, time_ix);
            y = pose(2, time_ix);
            theta = pose(3, time_ix);

            % one single measurement
            alfa = scanAngles(angle_ix,1);
            d = ranges(angle_ix, time_ix);

            % find the occupied spot
            [xocc, yocc] = locateOccupiedSpot( x, y, theta, alfa, d );

            % map world coordinates -> indices in map
            x_disc = ceil(x * resolution) + mapOrigin_x;
            y_disc = ceil(y * resolution) + mapOrigin_y;
            xocc_disc = ceil(xocc * resolution) + mapOrigin_x;
            yocc_disc = ceil(yocc * resolution) + mapOrigin_y;

            if ( x_disc >= 1 && x_disc <= param.size(1) && ...
                 y_disc >= 1 && y_disc <= param.size(2) && ...
                 xocc_disc >= 1 && xocc_disc <= param.size(1) && ...
                 yocc_disc >= 1 && yocc_disc <= param.size(2) )
                
                [freex, freey] = bresenham( x_disc, y_disc, xocc_disc, yocc_disc );

                % bresenham may give a zero size results
                if ( size(freex,2) == 1 )
                    theMap = updateOdds( param, theMap, freex, freey, xocc_disc, yocc_disc );
                end

            end
 
        end

    end

end

% pose of the lidar is (x,y,alpha)'
% d is the distance from the body to the obstacle
% theta is respect to the X axis attached to the body frame
function [xocc, yocc] = locateOccupiedSpot( x, y, theta, alfa, d )

    xocc = x + d * cos( theta + alfa );

    % the sign is wrong, it should positive, but if I make it right, the results are wrong,
    % therefore the input data is wrong, the signs are probably flipped for both theta and alpha
    yocc = y - d * sin( theta + alfa );
    
end

% freex and freey both M,1 discretized from the lidar (1,:) towards, yet not including, the occupied point
function outMap = updateOdds( param, map, freex, freey, xocc, yocc )

    assert ( size(freex,1) == size(freey,1) );
    assert ( size(freex,2) == size(freey,2) );
    assert ( size(freex,2) == 1 );
            
    for i = 1:size(freex,1)
        map(freey(i,1),freex(i,1)) = map(freey(i,1),freex(i,1)) - param.lo_free;
        if ( map(freey(i,1),freex(i,1)) < param.lo_min )
            map(freey(i,1),freex(i,1)) = param.lo_min;
        end
    end

    map(yocc,xocc) = map(yocc,xocc) + param.lo_occ;
    if ( map(yocc,xocc) > param.lo_max )
        map(yocc,xocc) = param.lo_max;
    end
    
    outMap = map;
    
end

