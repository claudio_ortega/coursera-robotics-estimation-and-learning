% Robotics: Estimation and Learning 
% WEEK 4
% 
%   ranges        [nMeasurements x nPoses] has one measurement per angle in the row direction, 
%                 with one point in time on the column direction
%   scanAngles    corresponds to the measurements in variable 'ranges' on the row direction, 
%                 and applies to all time samples, so it is only one column
%   map           rows in positive y direction, columns in positive x direction
%
function poses = particleLocalization(ranges, scanAngles, map, param)

    poses = particleLocalization2(ranges, scanAngles, map, param);
    
    save ( './output.mat', 'ranges', 'scanAngles', 'map', 'param' );
    
    plotResults(ranges, scanAngles, map, param, poses );

end

