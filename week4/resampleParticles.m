
function [particlesOut, weightsOut] = resampleParticles (particlesIn, weightsIn)

    numberOfParticlesIn = size( particlesIn, 2 );
    numberOfWeights = size( weightsIn, 2 );
    
    assert ( numberOfParticlesIn == numberOfWeights )
    
    normWeights = weightsIn / sum(weightsIn);

    % get the cummulative from weights
    cumWeights = cumsum( normWeights );

    particlesOut = zeros( 3, numberOfParticlesIn );
    weightsOut = zeros( 1, numberOfParticlesIn );

    for particle_ix = 1:numberOfParticlesIn

        r = rand();

        % find the bucket     
        found_ix = -1;
        for bucket = 1:numberOfWeights
            if r < cumWeights(bucket)
                found_ix = bucket;
                break;
            end
        end

        if found_ix == -1
            found_ix = numberOfWeights;
        end

        particlesOut(:,particle_ix) = particlesIn(:,found_ix);
        %weightsOut(:,particle_ix) = 1.0;
        weightsOut(:,particle_ix) = weightsIn(:,found_ix);
    end

    sumWeightsOut = sum( weightsOut );
    weightsOut = weightsOut / sumWeightsOut;
    
end

