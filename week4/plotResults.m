
function plotResults( ranges, scanAngles, M, param, pose )

    %  The final grid map:
    close all;
    figure;
    imagesc(M); 
    colormap('gray');
    hold on;

    %disp('Press any key to continue. (Ctrl+c to exit)')
    %pause

    %% Plot LIDAR data for ALL time samples
    nPoses = size(pose,2);
    lidar_end = zeros(size(scanAngles,1), 2);
    for time=1:nPoses
        plot(pose(1,time)*param.resol+param.origin(1), pose(2,time)*param.resol+param.origin(2), 'r.-');

        lidar_end(:,1) = (  ranges(:,time) .* cos(scanAngles + pose(3,time)) + pose(1,time))*param.resol + param.origin(1);
        lidar_end(:,2) = ( -ranges(:,time) .* sin(scanAngles + pose(3,time)) + pose(2,time))*param.resol + param.origin(2);

        lidarPlot=plot(lidar_end(:,1), lidar_end(:,2), 'g.'); 

        %pause(0.001)
        delete(lidarPlot)
    end

end
