% particlesIn:       holds all particles at one single time step [3 x nParticles]
% ranges:            nMeasurements x 1 
% correlationScores: for all particles, [1 x nParticles]
%
function correlationScores = updateMeasurement (particlesIn, ranges, scanAngles, map, param)

    mapOrigin_x = param.origin(1,1);
    mapOrigin_y = param.origin(2,1);
    resolution = param.resol;
    
    n_angles = size(scanAngles,1);
    n_particles = size(particlesIn,2);
    correlationScores = zeros(1,n_particles);
    
    for particle_ix = 1:n_particles

        correlationScore = 0;
        
        % we accumulate the score accross all the measurements
        for angle_ix = 1:n_angles

            if mod( angle_ix, param.consts.moduloForScans ) == 0

                % get the lidar position in world coordinates
                x = particlesIn(1, particle_ix);
                y = particlesIn(2, particle_ix);
                theta = particlesIn(3, particle_ix);

                % for each single measurement, get alfa and range
                alfa = scanAngles(angle_ix, 1);
                range = ranges(angle_ix, 1);

                % find the tip of the range measurement in world coordinates
                [xrange, yrange] = transformCoordinates( x, y, theta + alfa, range );

                % map from world coordinates into map indices
                x_disc = ceil(x * resolution) + mapOrigin_x;
                y_disc = ceil(y * resolution) + mapOrigin_y;
                xrange_disc = ceil(xrange * resolution) + mapOrigin_x;
                yrange_disc = ceil(yrange * resolution) + mapOrigin_y;

                if ( x_disc >= 1 && x_disc <= size(map, 2) && ...
                     y_disc >= 1 && y_disc <= size(map, 1) && ...
                     xrange_disc >= 1 && xrange_disc <= size(map, 2) && ...
                     yrange_disc >= 1 && yrange_disc <= size(map, 1) )

                    % the ray is all inside the map,
                    % now make the apportation of each measurement to the
                    % weight of the particle
                    tmp = getCorrelationFromMeasurement( map, x_disc, y_disc, xrange_disc, yrange_disc );
                    correlationScore = correlationScore + tmp;

                end

            end

        end

        correlationScores(:,particle_ix) = limitModule ( correlationScore, param.consts.limitCorrelationExp );
        
    end

end

function score = limitModule( inscore, limitModuleValue )
    if inscore < -limitModuleValue 
        score = -limitModuleValue;
    elseif inscore > limitModuleValue 
        score = limitModuleValue;
    else 
        score = inscore;
    end
end

% score as defined in .pdf for this asignment
% mapAprioriProb is the already considered truth mapOccupancy probability,
% currentRangeIsOccupied is tru iff the first arg is measured now as occupied
function score = getCorrelationFromMeasurement( aPrioriMap, x_disc, y_disc, xrange_disc, yrange_disc )

%    [freex, freey] = bresenham( x_disc, y_disc, xrange_disc, yrange_disc );

    % freex does not include xrange_disc
    % freey does not include yrange_disc

    score = 0;

%    if size( freex, 2 ) == 1 && size( freey, 2 ) == 1
%        % compute on non-occupied
%        for i = 1:size(freex,1)
%            score = score + scoreFromMeasurement( aPrioriMap(freey(i,1),freex(i,1)), false );
%        end
%    end

    % compute on occupied
    score = score + scoreFromMeasurement( aPrioriMap(yrange_disc, xrange_disc), true );

end

% score as defined in .pdf for this asignment
% aPrioriFromMap:        the a priori mapOccupancy probability,
% currentSpotIsOccupied: the measurement itself
function score = scoreFromMeasurement( aPrioriFromMap, measuredOccupied )

    if aPrioriFromMap > 0.7
        if measuredOccupied
            score = 10;
        else
            score = 0;
        end
    elseif aPrioriFromMap < 0
        if measuredOccupied
            score = 0;
        else
            score = 0;
        end
    else
        score = 0;
    end

end

% pose of the lidar is (x,y,alpha)'
% d is the distance from the body to the obstacle
% theta is respect to the X axis attached to the body frame
function [xocc, yocc] = transformCoordinates( x, y, rotationAngle, range )

    xocc = x + range * cos( rotationAngle );

    % the sign is wrong, it should positive, but if I make it right, the results are wrong,
    % therefore the input data is wrong, the signs are probably flipped for both theta and alpha
    yocc = y - range * sin( rotationAngle );

end

