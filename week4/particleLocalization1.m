% Robotics: Estimation and Learning 
% WEEK 4
% 
%   ranges        [nMeasurements x nPoses] has one measurement per angle in the row direction, 
%                 with one point in time on the column direction
%   scanAngles    corresponds to the measurements in variable 'ranges' on the row direction, 
%                 and applies to all time samples, so it is only one column
%   map           rows in positive y direction, columns in positive x direction
%
function poses = particleLocalization1(ranges, scanAngles, map, param)

    param.consts.delta_t = 1;   % is it 1 second?, why is this value absent in the .pdf?
    
    param.consts.F = [
                 [ 1  param.consts.delta_t  0        0                    ];
                 [ 0                     1  0        0                    ];
                 [ 0                     0  1        param.consts.delta_t ];
                 [ 0                     0  0        1                    ];
              ];
    
    % model covariance 4,4
    param.consts.sigma_x = 0.65;
    param.consts.sigma_y = 0.65;
    param.consts.sigma_theta = 10*pi/180;

    % particle filter specific parameters
    param.consts.nParticles = 100;
    param.consts.nParticlesResamplingThreshold = param.consts.nParticles * 0.70;
    param.consts.limitCorrelationExp = 40;
    param.consts.moduloForScans = 100;

    % the number of measurements in the time dimension ==> the number of poses in the time dimension to compute
    nPoses = size(ranges, 2);

    % each pose has x,y,attitude in the lab frame, each column per sampling time 
    % [x1; y1; theta1], [y1; y2; theta2], ...]
    poses = zeros(3, nPoses);

    % number of particles help in the filter
    nParticles = param.consts.nParticles;

    % The initial pose is a given
    poses(:,1) = param.init_pose;
    % You should put the given initial pose into first element in poses, and ignoring the measurements.
    % The pose(:,1) should be the pose when ranges(:,j) were measured.

    % initially we clone the first pose into 'nParticles' identical ones.
    % particles has x,y,theta rows, and one particle per column
    particles = repmat(poses(1:3,1), [1, nParticles]);
    
    % weights has one row, and one column per particle
    weights = zeros( 1, nParticles );
    weights = weights + 1/nParticles;

    % we have nPoses measurements, each measurement is a pair of lidar range/scanAngle values
    % we start estimating myPose from time=2 onwards
    for time = 2:nPoses 
        
        time
        
        %
        % 1) Propagate each of the particles using the same linear model with noise
        particles = updateModel(particles, param);

        %
        % 2) Measurement Update from measurements @(time-1)
        %   2-1) Find grid cells hit by the rays (in the grid map coordinate frame)
        %   2-2) For each particle, calculate the correlation scores of the particles
        scores = updateMeasurement(particles, ranges(:,time-1), scanAngles, map, param);

        %
        %   2-3) Update the particle weights with the latest measurements
        %weights = exp( scores ) .* weights;
        % .. and normalize them to sum=1
        weights = weights / sum(weights);
        
        %
        %   2-4) Compute the mean pose based on weights
        poses(:,time) = particles * weights';

        %
        % 3) Resample whenever the effective number of particles is under certain threshold
        nEffective = getEffectiveParticleCount( weights );

        if ( nEffective < param.consts.nParticlesResamplingThreshold )
            nEffective
            [particles, weights] = resampleParticles ( particles, weights );
        end
    end

end

