function myPose = particleLocalization2(ranges, scanAngles, map, param)

    N = size(ranges, 2);

    myPose = zeros(3, N);

    myResolution = param.resol;

    myOrigin = param.origin;

    myPose(:,1) = param.init_pose;

    M = 100;

    P = repmat(myPose(:, 1), [1, M]);
    W = ones(1, M) / M;
    ids = round(linspace(1, size(ranges, 1), 100));

    for j = 2:N

        j

        for i = 1:M
            dist = 0.025 + randn() * 0.1;
            heading = P(3, i) + randn() * 0.15;
            P(1, i) = P(1, i) + dist * cos(heading);
            P(2, i) = P(2, i) - dist * sin(heading);
            P(3, i) = heading;

            phi = P(3, i) + scanAngles(ids);

            x_occ = ranges(ids, j) .* cos(phi) + P(1, i);
            y_occ = -ranges(ids, j) .* sin(phi) + P(2, i);

            i_x_occ = ceil(myResolution * x_occ) + myOrigin(1);
            i_y_occ = ceil(myResolution * y_occ) + myOrigin(2);

            N_occ = size(i_x_occ, 1);
            corr = 0;
            for k = 1:N_occ
                if (i_x_occ(k) <= size(map, 2)) && (i_x_occ(k) >= 1 && i_y_occ(k) <= size(map, 1)) && (i_y_occ(k) >= 1)
                    if map(i_y_occ(k), i_x_occ(k)) > 0.7
                        corr = corr + 10;
                    end
                end
            end
            W(i) = W(i) + corr;
        end

        W_norm = W / sum(W);

        N_eff = floor( sum(W_norm) / sum(W_norm .^ 2) );

        [W_sorted, sort_ids] = sort(W_norm, 'descend');
        W_sample = W_sorted(1:N_eff);
        P_sorted = P(:, sort_ids);
        P_sample = P_sorted(:, 1:N_eff);

        myPose(:, j) = mean(P_sorted(:, 1:10), 2);

        p_ids = randsample(N_eff, M, 'true', W_sample);
        W = W_sample(p_ids');
        P = P_sample(:, p_ids');

    end

end
