function particlesOut = updateModel(particlesIn, param)

    % each particle should be hadled separately, their own dynamics
    numberOfParticlesIn = size( particlesIn, 2 );
    particlesOut = particlesIn;
    
    for particle_ix = 1:numberOfParticlesIn
        xNoise = sqrt(param.consts.sigma_x) * randn();
        yNoise = sqrt(param.consts.sigma_y) * randn();
        thetaNoise = sqrt(param.consts.sigma_theta) * randn();
        particlesOut(:,particle_ix) = particlesIn(:,particle_ix) + [xNoise; yNoise; thetaNoise];    
    end
    
    % pending:
    % use the transition matrix param.F to advance the state,
    % for which we do not know the value for delta_t (in param.F),
    % as it was not given in the instructions..

end
