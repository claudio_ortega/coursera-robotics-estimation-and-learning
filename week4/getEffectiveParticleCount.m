
function effCount = getEffectiveParticleCount(weights)

    weightSumSquared = sum( weights ) ^ 2;
    sumOfWeightsSquared = weights * weights';
    effCount = weightSumSquared / sumOfWeightsSquared;
    
end

