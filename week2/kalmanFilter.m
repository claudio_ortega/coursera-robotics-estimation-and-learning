
function [ predict_x, predict_y, state, param ] = kalmanFilter( t, measured_pos_x, measured_pos_y, state, param, previous_t )
%UNTITLED Summary of this function goes here
%   Four dimensional state: position_x, position_y, velocity_x, velocity_y

    % Check if the first time running this function
    if previous_t<0
    %% Place parameters like covarainces, etc. here:
        param.Q = 100 * eye(4);
        param.R = 0.1 * eye(2);
        param.P = param.Q;
        param.H = [ [1 0 0 0 ]; [0 1 0 0 ] ];
        param.I44 = eye(4);
        predict_x = measured_pos_x;
        predict_y = measured_pos_y;
        state = [measured_pos_x; measured_pos_y; 0; 0];
    else

        delta_t = t - previous_t;
        
        param.F = [ 
                     [ 1  delta_t  0        0 ]; 
                     [ 0        1  0        0 ];
                     [ 0        0  1  delta_t ];
                     [ 0        0  0        1 ];
                  ];

        % model covariance 4,4
        Q = param.Q;
        % observation covariance 2,2
        R = param.R;
        % transition matrix 4,4
        F = param.F;
        % observation matrix 2,4
        H = param.H;
        % covariance 4,4
        P = param.P;
        % state 4,1
        X = state;
        % observation
        Z = [ measured_pos_x; measured_pos_y ];
        % identity 4,4
        I = param.I44;

        %  see wikipedia
        %  https://en.wikipedia.org/wiki/Kalman_filter
        %

        % Predict phase
        X = F * X;
        P = ( F * P * F' ) + Q;

        % Update 
        
        % residual 2,1
        Y = Z - ( H * X );

        % Kalman Gain
        K = P * H' * inv ( R + ( H * P * H' ) );

        % update of state using observations
        X = X + ( K * Y );

        % covariance adjustment
        P = (I - K * H) * P * (I - K * H)' + ( K * R * K');
        
        % memoize stuf for the next loop
        param.P = P;

        % return values as they are expected
        predict_x = X(1) + X(3) * 0.330;
        predict_y = X(2) + X(4) * 0.330;
        state = X;
    end
end
