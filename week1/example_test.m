% Robotics: Estimation and Learning
% WEEK 1
% 
% This script is to help run your algorithm and visualize the result from it.
close all hidden
clear 

imagepath = './train';

for k=1:19
    
    close all hidden
    
    % Load image
    I_rgb = imread(sprintf('%s/%03d.png',imagepath,k));

        
    % Implement your own detectBall function
    [segI_ab, loc] = detectBall(I_rgb);

    fprintf ( "k:%d\n", k )
    disp('Press any key to continue. (Ctrl+c to exit)')
    pause
    
end


