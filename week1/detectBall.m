% Robotics: Estimation and Learning 
% WEEK 1
% 
% Complete this function following the instruction. 
function [segI_ab, loc] = detectBall(I_rgb)
% function [segI, loc] = detectBall(I)
%
% INPUT
% I       120x160x3 numerial array 
%
% OUTPUT
% segI    120x160 numeric array
% loc     1x2 or 2x1 numeric array

    I_lab=rgb2lab(I_rgb);
    I_ab=I_lab(:,:,2:3);

    % from example_read
    %
    mu = [-715.8; 3749.4];
    sigma = 1.0e+05 * [[ 1.4538   -1.4020];[-1.4020    8.1051]];
    thresholdRelative = 0.6;
   
    % Find ball-color pixels using your model  
    prob = computeGaussianProbOnColorMatrix( mu, sigma, I_ab );
   
    max( prob(:) );
    min( prob(:) );
    delta = max( prob(:) ) - min( prob(:) );
    
    thresholdAbsolute = (min( prob(:) + ( thresholdRelative * delta ) ) );
    segI_ab = prob > thresholdAbsolute;

    % Do more processing to segment out the right cluster of pixels.
    % You may use the following functions.
    %   bwconncomp
    %   regionprops
    % Please see example_bw.m if you need an example code.
    % Compute the location of the ball center
    %
    
    % http://www.mathworks.com/help/images/ref/bwconncomp.html
    bw_biggest = false(size(segI_ab));
    CC = bwconncomp(segI_ab);
    numPixels = cellfun(@numel,CC.PixelIdxList);
    [~,idx] = max(numPixels);
    bw_biggest(CC.PixelIdxList{idx}) = true;

    figure('Position', [0,0,500,500])
    imshow(I_rgb);
    hold on;

    imshow(bw_biggest); 
    hold on;

    % show the centroid
    % http://www.mathworks.com/help/images/ref/regionprops.html
    S = regionprops(CC,'Centroid');
    loc = S(idx).Centroid;

    plot(loc(1), loc(2),'r+');

    %
    % Note: In this assigment, the center of the segmented ball area will be considered for grading. 
    % (You don't need to consider the whole ball shape if the ball is occluded.)

    displayProb( prob )

end


function [probMatrix] = computeGaussianProbOnColorMatrix( mu, sigma, x )

    dim = size(x,3);
    % first layer is a, second layer is b
    assert ( dim == 2 )
    
    h = size(x,1);
    w = size(x,2);
    probMatrix = zeros( h, w );
    
    sigmainv = inv(sigma);
    detSigma = det( sigma );
    
    for i=1:h
        for j=1:w
            rgb = im2double( reshape( x(i,j,:), [], 1 ) );
            probMatrix(i,j) = computeGaussianProbOnVector( mu, sigma, sigmainv, detSigma, rgb );
        end
    end

end


function [prob] = computeGaussianProbOnVector( mu, sigma, sigmainv, detSigma, x )

    assert ( size(x,2) == 1 )
    assert ( size(sigma, 1) == size(x,1) )
    assert ( size(sigma, 2) == size(x,1) )
    assert ( isequal(size(mu),size(x) ) )
    
    d = size(x,1);
        
    ac = x-mu;
    exponent = - ( 0.5 * ac' * sigmainv * ac ); 
    coef = ( 1.0 / ( ( (2*pi) ^ d/2.0 ) * sqrt( detSigma ) ) );
    expo = exp ( exponent );
    prob =  coef * expo;

end

function displayProb( prob_ab )

    max_prob=max( prob_ab(:) );
    min_prob=min( prob_ab(:) );
    delta_prob = max_prob-min_prob;

    prob_ab = (-min_prob) + prob_ab;
    prob_ab = (1.0/delta_prob) .* prob_ab;

    h = size(prob_ab,1);
    w = size(prob_ab,2);

    x = zeros( h*w,1 );
    y = zeros( h*w,1 );
    z = zeros( h*w,1 );

    t = 1;
    for i=1:h
        for j=1:w
            x(t) = i;
            y(t) = j;
            z(t) = prob_ab(i,j);
            t = t + 1;
        end
    end

    figure('Position', [0,500,500,500])
    scatter3(x,y,z,'.')
    view(30,75)

end


