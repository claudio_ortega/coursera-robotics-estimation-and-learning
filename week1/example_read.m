% Robotics: Estimation and Learning 
% WEEK 3
% 
% This is an example code for collecting ball sample colors using roipoly
close all
clear all 

rgb_samples = table2array(readtable( "samples_rgb.txt" ));
N = size(rgb_samples,1);

% visualize the sample distribution
figure, 
scatter3(rgb_samples(:,1),rgb_samples(:,2),rgb_samples(:,3),'.');
title('Pixel Color Distribution - RGB');
xlabel('Red');
ylabel('Green');
zlabel('Blue');

hold on


lab_samples = rgb2lab(rgb_samples);
ab_samples = lab_samples(:, 2:3);

% visualize the sample distribution
figure, 
scatter(ab_samples(:,1),ab_samples(:,2),'.');
title('Pixel Color Distribution - ab');
xlabel('a');
ylabel('b');

% ML estimation for mu/sigma 
mu_rgb = (1.0/N) * sum(rgb_samples,1);
sigma_rgb = (1.0/N) * (rgb_samples-mu_rgb)' * (rgb_samples-mu_rgb);

mu_ab = (1.0/N) * sum(ab_samples,1)
sigma_ab = (1.0/N) * (ab_samples-mu_ab)' * (ab_samples-mu_ab)





