% Robotics: Estimation and Learning 
% WEEK 3
% 
% This is an example code for collecting ball sample colors using roipoly
close all

imagepath = './train';
Samples = [];
for k=1:15
    % Load image
    I = imread(sprintf('%s/%03d.png',imagepath,k));
    
    % You may consider other color space than RGB
    R = I(:,:,1);
    G = I(:,:,2);
    B = I(:,:,3);
    
    % Collect samples 
    %disp('');
    %disp('INTRUCTION: Click along the boundary of the ball. Double-click when you get back to the initial point.')
    %disp('INTRUCTION: You can maximize the window size of the figure for precise clicks.')
    figure(1), 
    mask = roipoly(I); 
    figure(2), imshow(mask); title('Mask');
    sample_ind = find(mask > 0);
    
    R = R(sample_ind);
    G = G(sample_ind);
    B = B(sample_ind);
    
    Samples = [Samples; [R G B]];
    
    fprintf ( "k:%d\n", k )
    
    disp( 'INTRUCTION: Press any key to continue. (Ctrl+c to exit)')
    pause
end

T = array2table (Samples, 'VariableNames', {'R','G','B'} );
writetable( T, "samples_rgb.txt" );
